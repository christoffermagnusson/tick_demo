# TICK demo README #


### What is this repository for? ###
* This repository was setup to share the demo setup used during the DevOps forum meeting. The presentation and demo were for TICK version 1.* and the container images are picked from stable versions on https://hub.docker.com/_/<service> (e.g telegraf).

### How do I get set up? ###
* Run ./init.sh to create .env file with required $PWD variable (if you already have this set, you are good to go)
* Make sure docker and docker-compose is installed and that docker engine is running. Check status with `sudo /etc/init.d/docker status` 
* Run docker-compose up (may need sudo)
* Grafana is setup to listen to the exposed port 3000
* Navigate to http://localhost:3000 and login with user admin and password admin

* Refer to the docs on https://docs.influxdata.com/ for telegraf, influxdb and kapacitor. For grafana check https://grafana.com/docs/grafana/latest/

* Owner: Christoffer Roodro
